const express = require('express');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const users = require('./app/users');
const tasks = require('./app/tasks');


const app = express();
app.use(express.json());

const port = 8000;


app.use('/users', users);
app.use('/tasks', tasks);

const run = async () => {
  await mongoose.connect('mongodb://localhost/todolist');

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  exitHook(() => {
    console.log('Successfully disconnected from the DB');
    mongoose.disconnect();
  });
};

run().catch(e => console.error(e));
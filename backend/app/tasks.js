const express = require('express');
const auth = require("../middleware/auth");
const Task = require("../models/Task");

const router = express.Router();

router.post('/', auth, async (req, res) => {
  if (!req.body.title) {
    return res.status(400).send('Data not valid');
  }

  const taskData = {
    title: req.body.title,
    description: req.body.description || null,
    status: 'new',
    user: req.user._id

  };

  const task = new Task(taskData);

  try {
    await task.save();
    res.send(task);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get('/', auth, async (req, res) => {
  try {
    const userId = req.user._id;
    const tasks = await Task.find({user: userId});
    res.send(tasks);
  } catch (e) {
    res.sendStatus(500);
  }
});
router.put('/:id', auth, async (req, res) => {

  if (!req.body.status) {
    return res.status(400).send('Status is absent');
  }

  const taskData = {
    title: req.body.title,
    description: req.body.description || null,
    status: req.body.status
  };

  try {
    const task = await Task.findOne({_id: req.params.id});

    if (!task.user.equals(req.user._id)) {
      res.status(403).send({message: 'You don\'t have permissions for this action'});
    }

    const editTask = await Task.findByIdAndUpdate(req.params.id, taskData, {new: true});
    if (!editTask) {
      res.status(404).send({message: 'Task not found'});
    }
    res.send(editTask);
  } catch (error) {
    res.status(500).send({message: 'Bad request'});
  }
});

router.delete('/:id', auth, async (req, res) => {

  try {

    const taskData = await Task.findOne({_id: req.params.id});
    if (!taskData.user.equals(req.user._id)) {
      res.status(403).send({message: 'You don\'t have permissions for this action'});
    }
    const task = await Task.findByIdAndDelete(req.params.id);

    if (!task) res.status(404).send({message: 'Task not found'});

    res.status(200).send({message: 'Successfully deleted ' + task.title + ' by id ' + task._id});

  } catch (error) {
    res.status(500).send({message: 'Bad request'});
  }
});

module.exports = router;